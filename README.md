Maven plugin for interacting with Redline Smalltalk.

Copyright (c) 2011 Robert Roland.
Released under the MIT License.
See the LICENSE file for more information.

Usage
-----

This Mojo currently has one goal: *stic*

It has two required configuration parameters:

 * sourceClass
 * sourcePath

It has one optional configuration parameter:
 
 * runtimePath

*sourceClass* is the Redline Smalltalk classname to execute (i.e. st.redline.HelloWorld)

*sourcePath* is the absolute path to this source file.

*runtimePath* is the absolute path to the Redline Smalltalk runtime library.  If this is not
specified, it will attempt to use REDLINE_HOME

See (https://github.com/robertrolandorg/redline-maven-plugin-sample/blob/master/pom.xml) for an example.